package com.epam.model;

public enum Pizza {
    NEAPOLITANA,
    CAPRICIOSA,
    MARGARITA,
    DIABOLA,
    HAWAIIAN
}
