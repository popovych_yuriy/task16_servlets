package com.epam.model;

public class Order {
    private static int unique = 1;
    private int id;
    private Pizza pizza;

    public Order(Pizza pizza) {
        this.pizza = pizza;
        id = unique;
        unique++;
    }

    public int getId() {
        return id;
    }

    public Pizza getPizza() {
        return pizza;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", pizza=" + pizza +
                '}';
    }
}
